# OpenML dataset: Crime_Data_from_2020_to_present_in_Los_Angeles

https://www.openml.org/d/45954

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
This dataset, named Crime_Data_from_2020_to_Present.csv, provides a detailed record of reported criminal incidents in a given area from the year 2020 onwards. It includes comprehensive information per incident, such as report numbers, reporting and occurrence dates and times, crime descriptions with specific codes, the locations (including area names and numbers, premises, LAT/LON coordinates), and details about the victims and suspects involved. This dataset is instrumental for analysts, public safety organizations, and researchers to understand crime patterns, allocate resources effectively, and develop crime prevention strategies.

Attribute Description:
- DR_NO: A unique identifier for the crime report.
- Date Rptd & DATE OCC: The dates when the crime was reported and occurred.
- TIME OCC: The time when the crime occurred.
- AREA & AREA NAME: Numeric and textual descriptions of the area where the crime occurred.
- Rpt Dist No: The reporting district number.
- Part 1-2: Indicates whether the crime is a Part 1 (more severe) or Part 2 offense.
- Crm Cd & Crm Cd Desc: The crime code and its description.
- Mocodes: Modus operandi codes related to the crime.
- Vict Age, Vict Sex, Vict Descent: Age, sex, and ethnic descent of the victim.
- Premis Cd & Premis Desc: Codes and descriptions of the premises where the crime occurred.
- Weapon Used Cd & Weapon Desc: Codes and descriptions of any weapons used.
- Status & Status Desc: The status of the crime report and its description (e.g., Invest Cont, Adult Arrest).
- Crm Cd 1-4: Additional crime codes related to the incident.
- LOCATION & Cross Street: The specific location and, if applicable, cross street of the crime.
- LAT & LON: Latitude and longitude of the crime location.

Use Case:
This dataset is crucial for public safety analyses, allowing for the tracking of crime trends, hotspot identification, and the assessment of law enforcement effectiveness. It can also be utilized by policymakers for strategic planning and by academic researchers studying the sociology of crime or developing predictive models. Community groups may use this data to advocate for safety and support initiatives in their neighborhoods.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45954) of an [OpenML dataset](https://www.openml.org/d/45954). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45954/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45954/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45954/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

